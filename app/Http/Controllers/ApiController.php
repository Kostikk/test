<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Illuminate\Support\Facades\DB;

use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller
{


    public $loginAfterSignUp = true;

    public function register(RegisterAuthRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }

        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }

    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;

        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'token' => $jwt_token,
        ]);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'logout OK'
            ],200);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Error, user cannot logout'
            ], 500);
        }
    }

    public function getAuthUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        $user = JWTAuth::authenticate($request->token);

        return response()->json(['user' => $user]);
    }

    public function index(/*Request $request*/)
    {
        $users = DB::table('users')->get();

//        $this->validate($request, [
//            'token' => 'required'
//        ]);
//        $user = JWTAuth::parseToken()->authenticate();
        return response()->json(['users' => $users]);
    }

    public function drop(Request $request, $id)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        $user = JWTAuth::authenticate($request->token);
        $deleteUser = DB::table('users')
            ->select()
            ->where('id', '=', $id);

        //$drop = DB::table('users')->where('id', '=', $id)->delete();


        if (!$deleteUser) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user with id '.$id.' cannot be found'
            ], 400);
        }

        if ($deleteUser->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'User could not be deleted'
            ], 500);
        }

    }
        public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

            $user = JWTAuth::authenticate($request->token);

            $editUser = DB::table('users')
            ->where('id', '=', $id)
            ->update(['name' => $request->get('name')]);



        if (!$editUser) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user with id ' . $id . ' cannot be found'
            ], 400);
        }

        if ($editUser) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'User could not be deleted'
            ], 500);
        }



    }
}
